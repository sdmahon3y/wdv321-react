import React from 'react';

function ContactButton(props) {
	return (
		<button className='friend'>
			<p>{props.contact.firstName}</p>
			<p>{props.contact.lastName}</p>
		</button>
	)
}

export default ContactButton;