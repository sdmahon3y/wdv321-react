import React from 'react';

import friends from './friends.js';
import ContactButton from './ContactButton.js';
import './App.css';

class App extends React.Component {
	removeFriend = () => {
		this.setState({
			currentFriend: null
		});
	};
	
	setCurrentFriend = (friend) => {
		console.log(friend);
		
		this.setState({ 
			currentFriend: friend
		});
	};

	constructor(props) {
		super(props);
	
		this.state = {
			currentFriend: null
		};
	}

	render() {
		return (
			<div className="App">
				{
					friends.map((person) =>
						<ContactButton
							contact={person}
						>
			
						</ContactButton>
					)
				}
				<div>
					<div>
						{
						this.state.currentFriend ? 
							<>
								<h1>{this.state.currentFriend.firstName}</h1>
								<h2>{this.state.currentFriend.lastName}</h2>
								<p>{this.statecurrentFriend.phoneNumber}</p>
								<p>{this.statecurrentFriend.city}</p>
								<p>{this.statecurrentFriend.state}</p>
								<button onClick={ () => this.setCurrentFriend(null)}>Close</button>
							</>
							: 
							<p>Please select a friend to view their details</p>
						}
					</div>
				</div>
			</div>
		);
	};
}

export default App;