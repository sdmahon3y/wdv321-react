export const friends = [
	{
	"firstName": "Jasmine",
	"lastName": "Francois",
	"phoneNumber": "15554443333",
	"addressLine1": "123 Main St",
	"addressLine2": "Apt #3",
	"city": "New York City",
	"state": "NY",
	"zip": "12345",
	"birthday": "12-12-1990"
	},
	{
	"firstName": "Nikko",
	"lastName": "Blazamut",
	"phoneNumber": "18884446666",
	"addressLine1": "357 Ocean Road",
	"addressLine2": null,
	"city": "Cedar Rapids",
	"state": "IA",
	"zip": "50096",
	"birthday": "10-19-1990"
	}
]

export default friends;